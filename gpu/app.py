from functools import reduce
from suds import WebFault
from suds.client import Client
from suds.sax.date import DateTime
from suds.transport import Reply
from suds.transport.https import HttpAuthenticated
from urllib.request import HTTPSHandler
from urllib.error import URLError

import datetime
import inspect
import io
import lxml.etree as etree
import re
import requests
import ssl
import suds
import sys
import hashlib

import logging
# logging.basicConfig(filename="/tmp/suds.log", level=logging.INFO)
# logging.getLogger('suds').setLevel(logging.DEBUG)

class ResquestTransport(HttpAuthenticated):
    """
    Classe destinada a resolver transport utilizando certificado de segurança.
    cert.crt.pem e cert.key.pem devem ser enviados por parametros.
    Para converter o p12 para o pem (key e crt) siga os comandos abaixo:

    Gerando o key:
    openssl pkcs12 -in <cert.p12> -out <file.key.pem> -nocerts -nodes

    Gerando o crt:
    openssl pkcs12 -in <cert.p12> -out <file.crt.pem> -clcerts -nokeys

    enviar crt e key por parametro nas chaves cert e key.
    """

    def __init__(self, **kwargs):
        self.cert = kwargs.pop('cert', None)
        self.key = kwargs.pop('key', None)
        HttpAuthenticated.__init__(self, **kwargs)

    def send(self, request):
        self.addcredentials(request)

        resp = requests.post(
                request.url,
                data=request.message,
                headers=request.headers,
                cert=(self.cert, self.key),
                verify=False)

        result = Reply(resp.status_code, resp.headers, resp.content)

        return result

class CreateXSD():
    _ns = "ns0:"
    _ct = "ct_"
    _st = "st_"

    client = None
    xsd = None

    message = None

    #valores para hash
    text_hash = ""

    def __init__(self):
        pass

    def get_value_xsd(self, xsd, attr, kv_vals):
        """
        Método com finalidade de retorna um valor valor de kv_vals conforme attr(chave) do xsd enviado.
        Irá, a partir do root do xsd enviado, alterar o valor de todas
        os elementos, desde que o mesmo tenha sido definido dentro de kv_vals
        Método recursivo utilizado em generate
        """
        #caso o elemento não tenha valor definido retorna o valor encontrado em kv_vals
        if xsd is None or isinstance(xsd, list):
            return kv_vals.get(attr)
        else:
            #se o valor não for None significa o xsd possui valor elementos,
            #com isso será definidio o valor de todos elementos, cujo valor exista em kv_vals
            #realizando a chamada do método get_value_xsd novamente
            attrs = xsd.__keylist__
            for x in attrs:
                #caso o elemento existente no XSD foi 'value' será definido o valor para ele
                #porém o valor existe em kv_vals esta registra na key do attr e não de x
                #por isso necessita de um tratamento especial
                #sintaxy setattr: setattr(elemento, localizacao, valor)
                if x == 'value':
                    valor = self.get_value_xsd(getattr(xsd, x), attr, kv_vals)
                else:
                    valor = self.get_value_xsd(getattr(xsd, x), x, kv_vals)
                if valor:
                    setattr(xsd, x, valor)
            return xsd

    def get_params_method(self):
        service = self.get_service_client()
        name_service = self.get_service_client().name

        #method
        method = self.get_method_client()

        #captura os input necessários para o método existente no client
        params = method.binding.input.param_defs(method)
        return params

    def create_xsd_to_param(self, param):
        if isinstance(param, str):
            xsd = self._ns+param
        else:
            xsd = self._ns+param.type[0]
        xsd = self.client.factory.create(xsd)
        return xsd

    def generate(self):
        #capturar o nome do método realizou a chamada do método
        #Desta maneira pode utilizar este método generate para gerar qualquer
        #XSD (complexType ou simplesType)
        # name_service = sys._getframe().f_back.f_code.co_name
        #altera a primeira letra do serviço solicitado para mínusculo, pois o
        #serviço pode ser configurado com a primeira letra maíscula porém o 
        #complexType (ct) foi configurado com letra mínuscula
        #TODO: verificar outra forma de fazer, buscando pelo suds
        #name_service = name_service[0].lower()+name_service[1:]
        #RESOLVE TODO:
        service = self.get_service_client()
        name_service = self.get_service_client().name
        # name_service = CreateXSD.get_service_client(client).name

        #captura os input necessários para o método existente no client
        params = self.get_params_method()

        #message a ser gerada completa
        message = {}

        #busca os paramentros passados para a função que realizou a chamada e 
        #as variaveis do escopo da função; com isso será possível setar os valores
        #dos campos do ct criado
        kvalores = inspect.getargvalues(sys._getframe().f_back).locals
        #remover valores None
        keys = kvalores.keys()
        for key in keys:
            if kvalores.get(key) is None:
                del kvalores[key]

        #concaterna as variaveis locais da função com os da classe (self) e com os
        #parametros enviados via args
        kvalores = {**kvalores, **vars(kvalores.get('self'))}
        #TODO: verificar se tem como buscar args de forma dinamica
        if 'args' in kvalores:
            kvalores = {**kvalores, **kvalores.pop('args')}

        for param in params:
            param = param[1]
            #criar o xsd conforme nome da subclasse de chamada
            #utiliza o suds para criar o factory do complexType (ct)
            #NOVO:busca o nome do parametro de forma automática
            xsd = self.create_xsd_to_param(param)

            #neste ponto será recuperado os valores padrões do XSD e os mesmos serão
            #agregados no contexto do kvalores, respeitando o valores informado pelo
            #chamada do método
            structure = self.get_properties(xsd.__class__.__name__)

            # for item in structure:
            for item in structure:
                children = item.get('children')
                if len(children) > 0:
                    for child in children:
                        kvalores = {**{child.get('name') : child.get('value') or child.get('default')}, **kvalores}
                        #converter valores numericos
                        if isinstance(kvalores.get(child.get('name')), list):
                            kvalores[child.get('name')] = kvalores[child.get('name')][0]
                        #remover valores None ou ''
                        if kvalores.get(child.get('name')) is None or kvalores.get(child.get('name')) == '':
                            del kvalores[child.get('name')]
                        # print("===>>", child.get('name'), "==>>", kvalores.get(child.get('name')))
                        # if child.get('tipo') == 'integer' and kvalores.get(child.get('name')):
                        #     print("kvalores", kvalores.get(child.get('name')))
                        #     kvalores[child.get('name')] = int(kvalores[child.get('name')])
                else:
                    kvalores = {**{item.get('name') : item.get('value') or item.get('default')}, **kvalores}
                    #converter valores numericos
                    if isinstance(kvalores.get(item.get('name')), list):
                        kvalores[item.get('name')] = kvalores[item.get('name')][0]
                    # remover valores None ou ''
                    if kvalores.get(item.get('name')) is None or kvalores.get(item.get('name')) == '':
                        del kvalores[item.get('name')]

                    # print("===>>", item.get('name'), "==>>", kvalores.get(item.get('name')))
                    # if item.get('tipo') == 'integer' and kvalores.get(item.get('name')):
                    #     print("kvalores", kvalores.get(item.get('name')))
                    #     kvalores[item.get('name')] = int(kvalores[item.get('name')])


            #captura as keys do xsd criado
            attrs = xsd.__keylist__
            #neste loop será definido os valores do items do XSD.
            #a função get_value_xsd é recursivo e consiguirár descer até último ramo
            #do elemento a ser definido valor
            #para os casos em que o xsd não possui atributos apenas o valor
            if not attrs:
                xsd = kvalores.get(param.name)
            else:
                for attr in attrs:
                    # if attr in kvalores:
                    valor = self.get_value_xsd(getattr(xsd, attr), attr, kvalores)
                    if valor:
                        setattr(xsd, attr, valor)
                    else:
                        # print("attr", attr, "xsd", xsd)
                        xsd.__delattr__(attr)
                        # print("XSD", xsd)

            message[param.name] = xsd

        return message

    def execute(self):
        """
        Este método foi criado com a finalidade de executar o serviço solicitado.
        É necessário em origin (classe que esta realizando chamada), possua um client 
        do tipo suds.client.Client
        """

        try:
            #captura o nome do método que realizou a chamada deste método
            # service = sys._getframe().f_back.f_code.co_name
            # service = service[0].upper()+service[1:]
            #TODO: verificar forma de buscar o client de forma dinamica
            # return eval("origin.client.service."+CreateXSD.get_method_client(origin.client).name+"(origin.cabecalho, origin.corpo, origin.hash)")
            return self.process_response(eval("self.client.service."+self.get_method_client().name+"(**self.message)"))
        except WebFault as e:
            if 'detail' in e.__dict__.get('fault').__dict__:
                return {'status':'error', 'message' : "Erro De Comunicação com Unimed Brasil. Entre em Contato com a T.I.!!!", 'error': e.fault.detail.erroInesperadoWS.erroInesperado.mensagemErro}
            else:
                return {'status':'error', 'message' : "Erro De Comunicação com Unimed Brasil. Entre em Contato com a T.I.!!!", 'error': e}
        except Exception as e:
            return {'status': 'error', 'message' : "Erro De Comunicação com Unimed Brasil. Entre em Contato com a T.I.!!! - Sem tratamento", 'error': str(e)}

    def get_service_client(self):
        """
        Este método só consegue retorna o serviço caso tenha apenas um serviço no
        caso constrário retona None e será necessário informar o nome do serviço.
            PARAM: client : suds.client.Client
            RETURN: service: suds.wsdl.Service or None
        """
        if len(self.client.wsdl.services) == 1:
            return self.client.wsdl.services[0]
        return None

    def get_port_service(self, service):
        """
        Este método somente retornará o port do service informado, caso no 
        serviço tenha um port. Caso contrário retornará None.
            PARAM: service : suds.wsdl.Service
            RETURN: port : suds.wsdl.Port or None
        """
        ports = list(service.ports)
        if len(ports) == 1:
            return ports[0]
        return None

    def get_method_port(self, port):
        """
        Este método somente retornará o method do port informado, caso no 
        port tenha um método. Caso contrário retornará None.
            PARAM: service : suds.wsdl.Port
            RETURN: method : String or None
        """
        methods = list(port.methods.keys())
        if len(methods) == 1:
            return port.methods[methods[0]]
        return None

    def get_method_client(self):
        """
        Retorna o valor do method a ser executado, somente retornará valor caso
        exista apenas um port, um service e um method
        """
        return self.get_method_port(self.get_port_service(self.get_service_client()))

    def create_element(self, elem, **init):
        """
        Conforme o client e elem (xsd) enviados por paramentros será criado um dict
        com os valores:
                required,
                name,
                label, => valor informado no name e separando as letras maíusculas
                children,
                origin,
                enum,
                value, => caso seja enum e possua apenas um valor possível
                default, => caso seja enum busca o primeiro valor como default
        """
        enum = []
        totalDigits = None
        maxLength = None
        pattern = None
        default = None
        value = None
        tipo = None
        if elem[0].type is not None:
            t = self.client.wsdl.schema.types.get(elem[0].type)
            value = init.get(elem[0].name)
            if t.enum():
                enum = self.client.factory.create(self._ns+elem[0].type[0]).__keylist__
                value = enum[0] if len(enum) == 1 else None
                default = enum[0]
            if 'totalDigits' in t.__dir__() and t.totalDigits is not None:
                totalDigits = t.totalDigits
            # if 'maxLength ' in t.__dir__() and t.maxLength is not None:
            #     maxLength = t.maxLength
            if 'pattern' in t.__dir__() and t.pattern is not None:
                pattern = t.pattern
            if t.restriction():
                tipo = t.rawchildren[0].ref[0]

        return {'required' : elem[0].required(),
                'label' : re.sub(r"(\w)([A-Z])", r"\1 \2",(elem[0].name)[0].upper()+(elem[0].name)[1:]),
                'name' : elem[0].name,
                'children' : [],
                'origin' : elem,
                'enum' : enum,
                'default' : default,
                'value' : value,
                'totalDigits' : totalDigits,
                'maxLength' : maxLength,
                'pattern' : pattern,
                'tipo' : tipo
                }

    def get_properties(self, name_obj, **init):
        """
        Retorna as propriedades do obj(XSD) enviados como parametros
        """
        properties = list()
        element = list(filter(lambda x: x[0] == name_obj , self.client.wsdl.schema.types))
        if element:
            elements = self.client.wsdl.schema.types.get(element[0]).children()
            for elem in elements:
                item = self.create_element(elem, **init)
                if len(elem[0].children()) > 0:
                    for chil in elem[0].children():
                        children = self.create_element(chil, **init)
                        item['children'].append(children)
                properties.append(item)
        return properties

    def valid(self, structure, value):
        validate = {'value' : value,
                    'node' : structure.get('name')}

        if structure.get('required') and not value:
            validate['error'] = 'required'
            return validate
        #
        # if structure.get('enum') and value not in structure.get('enum'):
        #     validate['error'] = 'enum'
        #     validate['valid'] = structure.get('enum')
        #     return validate
        #
        # if structure.get('totalDigits') is not None and len(str(value)) != int(structure.get('totalDigits')):
        #     validate['error'] = 'totalDigits'
        #     validate['valid'] = structure.get('totalDigits')
        #     return validate
        #
        # if structure.get('maxLength') is not None and len(str(value)) > int(structure.get('maxLength')):
        #     validate['error'] = 'maxLength'
        #     validate['valid'] = structure.get('maxLength')
        #     return validate
        #
        # if structure.get('pattern') is not None and value is not None and not re.match("^"+structure.get('pattern'), str(value)):
        #     validate['error'] = 'pattern'
        #     validate['valid'] = structure.get('pattern')
        #     return validate

        return validate

    def check_data(self, frame, root, parent=None):
        checked = []
        for structure in frame:
            node = structure.get('name')
            if node in root:
                node = root[node]
                if len(structure.get('children')) > 0:
                    for n in structure.get('children'):
                        if structure['enum']:
                            checked.append(self.valid(n, node.value))
                        else:
                            if n.get('name') in node:
                                checked.append(self.valid(n, node[n.get('name')]))
                else:
                    if structure['enum']:
                        checked.append(self.valid(structure, node.value))
                    else:
                        checked.append(self.valid(structure, node))
        valid = {'error': list(filter(lambda x: x.get('error'),checked)),
                'valid': list(filter(lambda x: x.get('error') is None, checked))}
        if valid.get('error'):
            valid['status'] = 'error'
        return valid

    def process_response(self, obj):
        data = {}
        try:
            #converter o obj em dict
            d_obj = suds.sudsobject.asdict(obj)
            keys = d_obj.keys()
            for k in keys:
                data[k] = self.process_response(d_obj[k])
            return data
        except ValueError or TypeError:
            if isinstance(obj, list):
                items = []
                for item in obj:
                    items.append(self.process_response(item))
                return items
            return str(obj)
        except Exception as e:
            return str(obj)

    def monta_text_hash(self, valor):
        text = ""
        if (not hasattr(valor, '__keylist__') and not isinstance(valor, dict)) or valor is None:
            if valor:
                # self.text_hash += str(valor)
                # print("text_hash", self.text_hash)
                return str(valor)
            return ""
        if len(valor) > 1:
            if hasattr(valor, '__keylist__'):
                fields = getattr(valor, '__keylist__')
            else:
                fields = valor.keys()
            for field in fields:
                # print("FIELD -> ", field)
                if hasattr(valor, '__keylist__'):
                    value = getattr(valor, field)
                else:
                    value = valor.get(field)
                text += self.monta_text_hash(value)
        else:
            text += self.monta_text_hash(valor[0])
        return text

    def calcula_hash(self):
        # reslist = list(self.envelope.iter())
        # pega somente os valores do xml
        # result = ''.join([element.text or '' for element in reslist])
        self.text_hash = ""
        for chave, valor in self.message.items():
            if chave != 'hash':
                self.text_hash += self.monta_text_hash(suds.sudsobject.asdict(valor))
        return hashlib.md5(self.text_hash.encode('ISO-8859-1')).hexdigest()

class Protocolo(CreateXSD):
    """
    Esta classe foi criada para gerenciar a solicitação de Protocolo Junto a Unimed Brasil (GPU).
    Muitos campos são obrigatórios pelo xsd definido pela empresa, porém estes valores,
    possuem apenas um valor possível, para este caso a lib irá inserir este valor automático.
    Input:
        codigoTransacao : String -> Valores possiveis ['001' -> Solicitação de Protocolo,
                                                       '006' -> Consultar Situação Protocolo,
                                                       '008' -> Consultar Historico]
        idUsuario : String -> Usuário que esta solicitando serviço
        codigoUnimedOrigemMensagem : String -> Código da Unimed que irá enviar requisição
        numeroTransacaoPrestadora : int -> AutoID da requisão, deve ser controlado pela unimed de origem
        configuracoes : dict -> Enviar mapa de configurações, as chaves utilizadas serão:
                                [tipoCliente, idUsuario, cert, cert_key]
                                Obrigatório enviar cert e cert_key, que serão requeidos para comunicação como client
    """

    servicos = {
            "001": {'wsdl': "http://gpuweb.unimed.coop.br/gpu-web/wsdls/hom/solicitarProtocolo_V1_1_0.wsdl",
                'service': "solicitarProtocolo"},
            "002" : {'wsdl': "http://gpuweb.unimed.coop.br/gpu-web/wsdls/hom/solicitarProtocolo_V1_1_0.wsdl",
                'element': "ns0:ct_respostaAtendimento"},
            "003": {'wsdl' : "http://gpuweb.unimed.coop.br/gpu-web/wsdls/hom/complementarProtocolo_V1_1_0.wsdl",
                'service': "ComplementoProtocolo"},
            "005": {'wsdl' : "http://gpuweb.unimed.coop.br/gpu-web/wsdls/hom/respostaAtendimento_V1_1_0.wsdl",
                'service': "respostaAtendimento"},
            "006": {'wsdl': "http://gpuweb.unimed.coop.br/gpu-web/wsdls/hom/consultaStatusProtocolo_V1_1_0.wsdl",
                'service': "ConsultaStatusProtocolo"},
            "008": {'wsdl': "http://gpuweb.unimed.coop.br/gpu-web/wsdls/hom/consultaHistorico_V1_1_0.wsdl",
                'service': "ConsultaHistorico"},
            "010": {'wsdl': "http://gpuweb.unimed.coop.br/gpu-web/wsdls/hom/cancelamento_V1_1_0.wsdl",
                'service' : "Cancelamento"}
                }


    servicos_prd = {
            "001": {'wsdl': "http://gpuweb.unimed.coop.br/gpu-web/wsdls/prd/solicitarProtocolo_V1_1_0.wsdl",
                'service': "solicitarProtocolo"},
            "002" : {'wsdl':  "http://gpuweb.unimed.coop.br/gpu-web/wsdls/prd/solicitarProtocolo_V1_1_0.wsdl",
                'element': "ns0:ct_respostaAtendimento"},
            "003": {'wsdl' : "http://gpuweb.unimed.coop.br/gpu-web/wsdls/prd/complementarProtocolo_V1_1_0.wsdl",
                'service': "ComplementoProtocolo"},
            "005": {'wsdl' : "http://gpuweb.unimed.coop.br/gpu-web/wsdls/prd/respostaAtendimento_V1_1_0.wsdl",
                'service': "respostaAtendimento"},
            "006": {'wsdl': "http://gpuweb.unimed.coop.br/gpu-web/wsdls/prd/consultaStatusProtocolo_V1_1_0.wsdl",
                'service': "ConsultaStatusProtocolo"},
            "008": {'wsdl': "http://gpuweb.unimed.coop.br/gpu-web/wsdls/prd/consultaHistorico_V1_1_0.wsdl",
                'service': "ConsultaHistorico"},
            "010": {'wsdl': "http://gpuweb.unimed.coop.br/gpu-web/wsdls/prd/cancelamento_V1_1_0.wsdl",
                'service' : "Cancelamento"}
                }


    def __init__(self, codigoTransacao, producao=False, *args, **kwargs):

        try:
            #caso seja em base de Producao
            if producao:
                self.servicos = self.servicos_prd

            CreateXSD.__init__(self)

            #dados especificos do serviço solicitado
            self.codigoTransacao = codigoTransacao
            self.wsdl = self.servicos[self.codigoTransacao]['wsdl']

            #captura os dados do certificado
            self.cert = kwargs.get('cert') or '/opt/cred/cert.crt.pem'
            self.cert_key = kwargs.get('cert_key') or '/opt/cred/cert.key.pem'

            #criar o cliente para realizar a comunicação
            if self.transport:
                self.client = Client(self.wsdl, transport=self.transport, cache=None)
            else:
                self.client = Client(self.wsdl, cache=None)
            
            self.client.set_options(headers={'Content-Type': 'text/xml; charset=ISO-8859-1', 'SOAPAction': b'""'})

            #para definir que somente será gerada a estrutura
            self.element = self.servicos.get(codigoTransacao).get('element')

            #busca o name do serviço existente no client
            self.servico = self.get_service_client().name
            if not self.servico:
                self.servico = self.servicos[codigoTransacao]['service']

            #buscar o método que será executado
            self.metodo = self.get_method_client()
            if not self.metodo:
                self.metodo = self.servicos[codigoTransacao]['method']

            #data da geração será data e hora atual
            self.dataGeracao = DateTime(datetime.datetime.today())

            #montar a estrutura da mensagem, conforme o metodo utilizado
            self.estrutura_mensagem = {}
            self.set_dados(**kwargs)

            for param in self.get_params_method():
                self.estrutura_mensagem[param[0]] = self.get_properties(param[1].type[0], **{**kwargs, **self.__dict__})
        except URLError as e:
            raise Exception("Erro ao Conectar Unimed Brasil! Tente Mais Tarde!")

    def verifica_novo_servico(self):
        quem_chamou = sys._getframe().f_back.f_code.co_name.lower()
        return not(self.client is not None and self.client.sd[0].service.name.lower() == quem_chamou)

    def tratar_carteira(self, carteira):
        if carteira is not None:
            self.codigoIdentificacao = carteira[4:]
            #self.codigoIdentificacao = carteira[4:].rjust(13,"0")
            self.codigoUnimed = carteira[:4]
            self.codigoUnimedDestinoMensagem = carteira[:4]

    # @property
    # def cabecalhoTransacao(self):
    #     return CreateXSD.generate(self.client)
    #
    # def corpoTransacao(self, **args):
    #     exec("""def %s(self, **args):
    #                 return CreateXSD.generate(self.client)"""% self.servico)
    #     return eval("""%s(self, **args)""" %self.servico)

    # @property
    # def executeTransacao(self):
    #     exec("""def %s(self):
    #                 return CreateXSD.execute(self)"""%self.servico)
    #     return eval("""%s(self)"""%self.servico)

    @property
    def transport(self):
        #'/opt/cred/cert.crt.pem'
        #'/opt/cred/cert.key.pem'
        if None in (self.cert, self.cert_key):
            return None
        return ResquestTransport(cert=self.cert, key=self.cert_key)

    def set_dados(self, **args):
        #separa os dados da carteira, preenche o codigoIdentificacao, codigoUnimed e codigoUnimedDestinoMensagem
        self.tratar_carteira(args.get('carteira'))

        # self.idUsuario = args.get('idUsuario')
        # self.codigoUnimedOrigemMensagem = args.get('codigoUnimedOrigemMensagem')
        # self.numeroTransacaoPrestadora = args.get('numeroTransacaoPrestadora')

        #define os dados de cabecalho e corpo da mensagem a ser enviada
        self.message = self.generate()

    def validar_dados(self, **args):
        #caso esteja realizando a chamada direta deste método, é importante definir
        #os valores enviados para validação
        #no fluxo comum da classe esta validação, está etapa será realizada no executa_servico
        if self.message is None:
            self.set_dados(**args)

        data = {}
        for k in self.message:
            checked = self.check_data(self.get_properties(self.message[k].__class__.__name__), self.message[k])
            data = {**{'error' : checked['error'],
                    'valid' : checked['valid']}, **data }

        data['status'] = 'error' if data['error'] else 'success'
        return data

    def executa_servico(self, **args):
        """
        Necessário informar código do serviço deseja executar, conforme atributo 
        serviços, este código é o mesmo do codigoTransacao (vide manual do GPU)
        """
        #define os dados enviados por parametros no self do objeto
        self.set_dados(**args)

        #validar os dados informados para executar o serviço
        validacao = self.validar_dados()

        if validacao['error']:
            return validacao

        self.hash = self.calcula_hash()
        self.message["hash"] = self.hash
        return self.execute()

    def get_propriedades_form(self):
        return self.estrutura_mensagem
        # corpo = self.corpoTransacao()
